$( function () {
	var roar = new Sinc({ title: 'Demo!', timeout: 3000});
	setTimeout( function () {
		roar.clear();
	}, 2000);

	setTimeout( function () {
		roar.message({ status: 'Success', title: 'Scheduler', message: 'Values saved successfully.', stayAlive: true});
	}, 3000);

	setTimeout( function () {
		roar.message({ status: 'Error', title: 'Scheduler', message: 'Oops, something is not right.', timeout: 1000}, function () {
			//roar.clear();
		});
	}, 10000);

	setTimeout( function () {
		roar.message( {status: 'Warning', title: 'Scheduler', message: 'Fetching latest data ...'});
	}, 6000);

	setTimeout( function () {
		roar.input( {status: 'Info', title: 'Scheduler', message: 'What did you say your name was?'});
	}, 6000);

	setTimeout ( function () {
		roar.confirm( {status: 'Info', title: 'Scheduler', message: 'New data available. Want us to reload the analytic?', stayAlive: true}, function (hasConfirmed) {
			if (hasConfirmed === true ) {
				roar.message( { status : 'Warning', title: 'Scheduler', message : 'This is taking longer than expected.'});
			}
		});
	}, 4000);

});

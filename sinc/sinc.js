var Sinc = function (settings) {
	var self = this;
	self.msgTitle = settings.title;
	self.timeout = settings.timeout;

	self.container = $('ul.roars');
	if (self.container.length === 0) {
		self.container = $('<ul class="roars"></ul>');
		$('body').append(self.container);
	}

	self.setInteractions();
};

Sinc.prototype = {
	message : function (config, cb) {
		var self = this,
			msgTemplate = Sinc.templates.message;

		config.title = (config.title || self.msgTitle);

    	var message = $(Mustache.render(msgTemplate, config)).hide();
		self.container.prepend(message);
		message.slideDown('slow');

		if (config.stayAlive !== true) {
			setTimeout( function () {
				message.slideUp('slow', function () {
					if (typeof cb === 'function') {
						cb();
					}
				});
			}, config.timeout || self.timeout || 5000);
		}

		return message;
	},

	confirm : function (config, cb) {
		var self = this,
        cfmTemplate = Sinc.templates.confirm;

		config.title = config.title || self.msgTitle;
		config.confirmBtnText = config.confirmBtnText || "Confirm";
		config.cancelBtnText = config.cancelBtnText || "Cancel";

		var confirmBox = $(Mustache.render(cfmTemplate, config)).hide();
		self.container.prepend(confirmBox);
		confirmBox.slideDown('slow');

		confirmBox.on('click', '.roar-ok', function () {
			confirmBox.slideUp('slow', function () {
				confirmBox.remove();
			});

			if (typeof cb === 'function') {
				cb(true);
			}
		});

		confirmBox.on('click', '.roar-cancel', function () {
			confirmBox.slideUp('slow', function () {
				confirmBox.remove();
			});

			if (typeof cb === 'function') {
				cb(false);
			}
		});

		return confirmBox;
	},

	input : function (config, cb) {
		var self = this,
			inputTemplate = Sinc.templates.input;

		config.title = config.title || self.msgTitle;
		config.confirmBtnText = config.confirmBtnText || "Ok";

		var inputBox = $(Mustache.render(inputTemplate, config)).hide();
		self.container.prepend(inputBox);
		inputBox.slideDown('slow', function () {
			$(this).find('input').focus();
		});

		inputBox.on('click', '.roar-ok', function () {
			inputBox.slideUp('slow', function () {
				inputBox.remove();
			});

			if (typeof cb === 'function') {
				cb(inputBox.find('input').val());
			}
		});

		return inputBox;
	},

	clear : function () {
		var self = this;
		self.container.find('.roar').slideUp('slow', function () {
			$(this).remove();
		});
	},

	setInteractions : function () {
		var self = this;

		self.container.on('click', 'img.roar-close', function () {
			$(this).closest('li.roar').slideUp('slow', function () {
				$(this).remove();
			});
		});
	}
};

Sinc.templates = {
	message : '	<li class="roar {{status}}"> \
				<div class="roar-icons"></div> \
				<div class="roar-content"> \
					<div class="roar-title">{{title}}</div> \
					<div class="roar-msg">{{message}}</div> \
				</div> \
				<div class="roar-actions"> \
					<img class="roar-img roar-close" src="../icons/times.png" /> \
				</div> \
			</li>',

	confirm : ' <li class="roar {{status}}"> \
				<div class="roar-icons"></div> \
				<div class="roar-content"> \
					<div class="roar-title">{{title}}</div> \
					<div class="roar-msg">{{message}}</div> \
				</div> \
				<div class="roar-actions"> \
					<img class="roar-img roar-close" src="../icons/times.png" /> \
					<div class="roar-ua"> \
						<button class="roar-btn roar-ok">{{confirmBtnText}}</button> \
						<button class="roar-btn roar-cancel">{{cancelBtnText}}</button> \
					</div> \
				</div> \
			</li>',

	input : ' <li class="roar {{status}}"> \
				<div class="roar-icons"></div> \
				<div class="roar-content"> \
					<div class="roar-title">{{title}}</div> \
					<div class="roar-msg">{{message}}</div> \
				</div> \
				<div class="roar-actions"> \
					<img class="roar-img roar-close" src="../icons/times.png" /> \
					<div class="roar-ua"> \
						<input class="roar-input" type="text" placeholder="{{placeholder}}"></input> \
						<button class="roar-btn roar-ok">{{confirmBtnText}}</button> \
					</div> \
				</div> \
			</li>'
};

Sinc.settings = {
	timeout : 2000
};

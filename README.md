backyard
========

This is a space where all my experiments go in, some of them may turn into projects, some of them may perish in here. Feel free to check these stuff out if you want.

### Proposed Ideas
- Sick application killer (An ultra slick easy to user process killer)
- Dynamic data visualization (A new approach to visualizing data)

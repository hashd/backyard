var tableTools = (function (window, document, undefined) {
    var downloadFile = function (data, fileName, fileFormat, mimeType) {
        var a = document.createElement('a');
        mimeType = mimeType || 'application/octet-stream';
        fileName = fileName + fileFormat;

        // Method 1 : IE10
        if (navigator.msSaveBlob) {
            return navigator.msSaveBlob(new Blob([data], {type: mimeType}, fileName));
        }

        // Method 2 : If html5 a [download] exists
        if ('download' in a) {
            var blob = new Blob([data], {type: mimeType}, fileName),
                xlsUrl = URL.createObjectURL(blob);

            a.href = xlsUrl;
            a.setAttribute('download', fileName);
            a.innerHTML = "Downloading ...";

            document.body.appendChild(a);
            setTimeout(function () {
                a.click();
                document.body.removeChild(a);
            }, 60);
            return true;
        }

        // Method 3 : else revert to Old method for older browsers, fileName is not supported in this case
        var f = document.createElement("iframe");
        document.body.appendChild(f);
        f.src = "data:" +  mimeType   + "," + encodeURIComponent(data);

        setTimeout(function() {
            document.body.removeChild(f);
        }, 300);
        return true;
    };

    //Either takes the table id or table node along with the fileName
    var exportTableAsExcel = function (tableElmt, fileName) {
        var mimeType = 'application/vnd.ms-excel;base64',
            fileFormat = 'xls',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"> \
                        <head><!--[if gte mso 9]> \
                        <xml>\
                            <x:ExcelWorkbook>\
                                <x:ExcelWorksheets>\
                                    <x:ExcelWorksheet>\
                                        <x:Name>{worksheet}</x:Name>\
                                        <x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions>\
                                    </x:ExcelWorksheet>\
                                   </x:ExcelWorksheets> \
                            </x:ExcelWorkbook> \
                        </xml><![endif]--> \
                        </head> \
                        <body> \
                            <table>{table}</table> \
                        </body> \
                        </html>';

        var format = function (s, c) {
            return s.replace(/{(\w+)}/g, function(m, p) {
                return c[p];
            })
        };

        if (!tableElmt.nodeType) {
            tableElmt = document.getElementById(tableElmt);
        }

        downloadFile(format(template, {
            worksheet : 'Worksheet',
            table : tableElmt.innerHTML
        }), fileName, fileFormat, mimeType);
    }

    return {
        exportTableAsExcel : exportTableAsExcel,
        exportTableAsCsv : undefined
    }
})(window, document);

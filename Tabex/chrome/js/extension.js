var exportTableAsExcel = function () {
    for (var i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
};

var exportTableAsCsv = function () {
    for (var i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
};

chrome.contextMenus.create({
    "id" : "tt-parent",
    "title" : "Table Tools",
    "contexts" : ["all"]
});

chrome.contextMenus.create({
    "id" : "tt-export-xls",
    "title" : "Export as Excel",
    "parentId" : "tt-parent"
});

chrome.contextMenus.create({
    "id" : "tt-export-csv",
    "title" : "Export as CSV",
    "parentId" : "tt-parent"
});

function onClickHandler(info, tab) {
    for (var i = 0; i < arguments.length; i++) {
        console.log(arguments[i]);
    }
};
chrome.contextMenus.onClicked.addListener(onClickHandler);
